import torch
import os
import itertools
import numpy as np
from multiprocessing import pool
import functools
from pre_processing import *
def parallel_indexing(frame_id,cells,frame,sequ_len):
    gab = 6
    index=[]
    relevant_index = [np.where(frame[:] == j)[0] for j in range(frame_id, frame_id + gab * sequ_len, gab)]
    for j in relevant_index[0]:
        cell_id = cells[j]
        cell_ind = [cell_id, [j]]
        # search for in same cell_id in next frame
        for jj in range(1, sequ_len):
            nex_frame_within_relevant = np.where(cells[relevant_index[jj]] == cell_id)[0]
            if (len(nex_frame_within_relevant) == 0):
                break
            cell_ind[1].append(relevant_index[jj][nex_frame_within_relevant[0]])
        index.append(cell_ind)
    return index
def create_index_for_sequnece(cells, frame, sequ_len,nb_workers=15):
    unique_frames = np.unique(frame[:])
    func = functools.partial(parallel_indexing,cells=cells,frame=frame,sequ_len=sequ_len)
    p=pool.Pool(nb_workers)
    index=p.map(func, unique_frames[:-sequ_len])
    p.close()
    p.join()
    index = list(itertools.chain(*index))
    return index
class Dataset(torch.utils.data.Dataset):
    def __init__(self, filename,sequences=True,seqlen=2):
        h5_file=filename+'.h5' if(not sequences) else filename + 'seq.h5'
        if(not os.path.exists(h5_file)):
            print('creating h5 data file')
            creat_h5_datasets(filename,sequences)
            print('h5 data file is created')
        h5f = h5py.File(h5_file, 'r')
        data= h5f['features'][:,:]
        self.mask = np.ones(data.shape[1], dtype=bool)
        not_taken=np.where((np.isinf(data.max(axis=0))))[0]
        self.mask[not_taken]=0
        print('removing '+str(len(self.mask)-sum(self.mask))+' columns')
        self.seqlen=seqlen
        self.mean=data[:,self.mask].mean(axis=0)
        self.std=data[:,self.mask].std(axis=0)
        self.data=h5f['features']
        self.labels=h5f['labels']
        if(sequences):
            print('Loading sequences indeces')
            self.inds=create_index_for_sequnece(h5f['Cell_id'][:],h5f['frame'][:],seqlen)
            print('Loading is done')
        self.seq=sequences
        self.data=h5f['features'][:,:]
        self.labels=h5f['labels'][:]
    def __len__(self):
        if(self.seq):
            return len(self.inds)
        return self.data.shape[0]
    def __getitem__(self, index):
       if(self.seq):
            to_fill=self.seqlen-len(self.inds[index][1])
            y=[ self.labels[i]-1 for i in self.inds[index][1]]
            x = [ (self.data[i][self.mask] - self.mean) / self.std for i in  self.inds[index][1]]
            return torch.cat((torch.tensor(x),torch.zeros((to_fill,x[0].shape[0]))) ), torch.cat((torch.tensor(y),torch.full((to_fill,),-1,dtype=torch.long)) )
       y=self.labels[index]-1
       x=(self.data[index][self.mask]-self.mean)/self.std
       return torch.tensor(x), torch.tensor(y)

