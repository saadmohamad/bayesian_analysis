import numpy as np
import h5py
import pandas as pd
from pgmpy.estimators import BDeuScore, K2Score, BicScore
import networkx as nx
import pylab as plt
from pgmpy.estimators import PC, HillClimbSearch, ExhaustiveSearch
from sklearn.preprocessing import KBinsDiscretizer
from dataset_load import Dataset
from pgmpy.models import BayesianModel,MarkovModel
from pgmpy.sampling import BayesianModelSampling
from pgmpy.inference import BeliefPropagation
from scipy import interpolate


class BayeData(Dataset):
    def __init__(self, filename,interpretable_faeture,seq=True):
        super(BayeData, self).__init__(filename)
        with open(filename, 'r') as ff:
            features = np.array(ff.readline().split(',')[:-2])
            taken = np.where([fea in interpretable_faeture for fea in features])[0]

        self.feature_name=features[taken]
        h5_file = filename + '.h5' if (not seq) else filename + 'seq.h5'
        h5f = h5py.File(h5_file, 'r')
        data = h5f['features'][:, taken]


        self.mean = data.mean(axis=0)
        self.std = data.std(axis=0)

        self.data = data[:, :]
        self.labels = h5f['labels'][:]


    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, index):
        y = self.labels[index] - 1

        x = (self.data[index] - self.mean) / self.std
        return x, y
def querying_2_variable_join(variables,model,evidence):
    belief_propagation = BeliefPropagation(model)

    return  belief_propagation.query(variables=variables,evidence=evidence),belief_propagation.query(variables=variables,evidence=evidence,joint=False)


def mutula_info(variable1,variable2,model,evidence=None):
    join, margi=querying_2_variable_join([variable1,variable2],model,evidence)
    variable1=join.variables[0]
    variable2=join.variables[1]
    marg_prod=np.matmul(margi[variable1].values.reshape(len(margi[variable1].values),1),margi[variable2].values.reshape(1,len(margi[variable2].values)))
    res = join.values * np.log(join.values / marg_prod)
    res[np.where(join.values == 0)] = 0
    muinf=np.sum(res)
    return muinf


def mutula_info_values(variable1,model,evidence):
    join, _=querying_2_variable_join([variable1],model,evidence)
    _, margi = querying_2_variable_join([variable1], model, None)
    variable=join.variables[0]
    res = join.values * np.log(join.values / margi[variable].values)
    res[np.where(join.values == 0)] = 0
    muinf=np.sum(res)
    return muinf
def mutula_info_dyn_values(variable1,model,evidence):
    join, _=querying_2_variable_join([variable1],model,evidence)
    evidence.pop('label1')
    _, margi = querying_2_variable_join([variable1], model, evidence)
    variable=join.variables[0]
    res = join.values * np.log(join.values / margi[variable].values)
    res[np.where(join.values == 0)] = 0
    muinf=np.sum(res)
    return muinf


def discretise_data(data_loader,nb_bin,encode='ordinal', strategy='quantile'):
    est = KBinsDiscretizer(n_bins=nb_bin, encode=encode, strategy=strategy)
    data=np.array([x for x,_ in data_loader])
    est.fit(data)
    return est.transform(data)
def correlation_for_markov(model,sample_Size,evidence=None):
    samples = BayesianModelSampling(model).forward_sample(size=sample_Size)
    if(evidence is not None):
        samples=samples.query(evidence)
        samples.drop(evidence.split(' == ')[0], inplace=True, axis=1)

    feature_name={ feature:ind  for ind,feature in enumerate(samples.columns)}
    covariance_matrix=np.cov(samples.transpose())
    edges_pair=[i for i in model.to_markov_model().edges()]
    correlation_graphs={}
    for pair in edges_pair:
        if(evidence is not None):
            if(evidence.split(' == ')[0] in pair):
                continue
        indy=feature_name[pair[0]]
        indx=feature_name[pair[1]]
        correlation=covariance_matrix[indy,indx]/((covariance_matrix[indy,indy]**0.5)*(covariance_matrix[indx,indx]**0.5))
        correlation_graphs.update({pair:{'weight':abs(correlation),'color':'Green' if(correlation>0) else 'red'}})
    return correlation_graphs


    #Xt = est.transform(X)
def create_graph(correlation,layout):
    G = nx.Graph()
    for key,values in correlation.items():
        G.add_edge(key[0],key[1],weight=values['weight']*10,color=values['color'])
    if(layout=='spring'):
        pos = nx.spring_layout(G)
    elif(layout=='spectral'):
        pos=nx.spectral_layout(G)
    elif (layout == 'shell'):
        pos = nx.shell_layout(G)
    elif(layout=='spiral'):
        pos=nx.spiral_layout(G)
    elif(layout=='circular'):
        pos=nx.circular_layout(G)
    else:
        pos=nx.random_layout(G)
    colors = nx.get_edge_attributes(G, 'color').values()
    weights = nx.get_edge_attributes(G, 'weight').values()
    nx.draw(G, pos,
            edge_color=colors,
            width=list(weights),
            with_labels=True,
            node_color='lightgreen')
    labels = {e: round(G.edges[e]['weight'],2) for e in G.edges}
    nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)

def create_dynamic_bayesian_network(features):

    g=BayesianModel()
    #g.add_nodes_from(features)
    for t in range(2):
        for fea in features:
            if(fea == 'class'):
                continue
            g.add_edges_from([('label'+str(t),fea+str(t))])
    g.add_edges_from([('label0','label1')])

    return g


def main():

    inter_featu=['green', 'red', 'area_cell', 'distance_centroid_colony', 'distance_border_colony',
                 'eccentricity', 'short_axis', 'long_axis', 'roundness','cell_speed','cell_size_change', 'density140','apoptosis_probability','mitosis_probability','class']
    data=BayeData('/home/sm20/Ed_project_data/Dataset2InterpretableFeaturesWithFrameCellSpeedandNuclearSizeChangeFrameLabelCorrect.csv',inter_featu)

    disretised_Data=discretise_data(data,400,strategy='quantile')






    #########################################################################################################################




    #sample=pd.DataFrame(data=disretised_Data,columns=data.feature_name)
    sample=pd.DataFrame(data=np.concatenate((disretised_Data,np.expand_dims(data.labels-1,axis=1)),axis=1),columns=np.concatenate((data.feature_name,np.array(['label'])),axis=0))
    #sample=sample.query('label==0')
    print('learning network strucutre')
    hc = HillClimbSearch(sample, scoring_method=BicScore(sample))
    best_model = hc.estimate()

    baye_best_model=BayesianModel(ebunch=best_model.edges())
    markov_best_model=baye_best_model.to_markov_model()
    #le = MaximumLikelihoodEstimator(baye_best_model, sample)
    print('learning network parameters')
    baye_best_model.fit(sample)
    plt.figure(2)
    nx.draw(markov_best_model, with_labels=True)
    plt.figure(3)
    nx.draw(baye_best_model, with_labels=True)
    plt.show()
################################################################################################################################
    print('task 1')
    print('Compute features improtance to the class')
    mutula_i={}
    for var in baye_best_model.nodes:
        if(var=='label'):
            continue
        mutula_i.update({var:mutula_info(var,'label',baye_best_model)})
    print(mutula_i)
    plt.bar(list(mutula_i.keys()), list(mutula_i.values()))
    plt.xticks(fontsize=6)
    plt.xlabel('Features')
    plt.ylabel('Feature importance')
    print('computing covariance')
    correlation = correlation_for_markov(baye_best_model, 5000)
    correlation_class_1=correlation_for_markov(baye_best_model,5000,evidence='label == 0')
    correlation_class_2 = correlation_for_markov(baye_best_model, 5000, evidence='label == 1')
    correlation_class_3 = correlation_for_markov(baye_best_model, 5000, evidence='label == 2')
    correlation_class_4 = correlation_for_markov(baye_best_model, 5000, evidence='label == 3')

    print('print graph')
    plt.figure('all_classes_1')
    create_graph(correlation,'circular')
    plt.figure('all_classes_2')
    create_graph(correlation, 'spring')
    plt.figure('classes pluri_1')
    create_graph(correlation_class_1,'circular')
    plt.figure('classes pluri_2')
    create_graph(correlation_class_1,'spring')
    plt.figure('classes ecto_1')
    create_graph(correlation_class_2,'circular')
    plt.figure('classes ecto_2')
    create_graph(correlation_class_2,'spring')
    plt.figure('classes endo_1')
    create_graph(correlation_class_3,'circular')
    plt.figure('classes endo_2')
    create_graph(correlation_class_3,'spring')
    plt.figure('classes Miso_1')
    create_graph(correlation_class_4,'circular')
    plt.figure('classes Miso_2')
    create_graph(correlation_class_4,'spring')
    plt.show()



###################################################################################################################################
    print('task 2')
    print('Compute features improtance to the class')
    mutula_i={}
    for var in baye_best_model.nodes:
        if(var=='label'):
            continue
        mutula_i.update({var:mutula_info_values(var,baye_best_model,{'label':0})})
    print(mutula_i)
    plt.figure('pluri_feartures_importance')
    plt.bar(list(mutula_i.keys()), list(mutula_i.values()))
    plt.xticks(fontsize=6)
    plt.xlabel('Features')
    plt.ylabel('Pluri Feature importance')



    mutula_i={}
    for var in baye_best_model.nodes:
        if(var=='label'):
            continue
        mutula_i.update({var:mutula_info_values(var,baye_best_model,{'label':1})})
    print(mutula_i)
    plt.figure('ecto_feartures_importance')
    plt.bar(list(mutula_i.keys()), list(mutula_i.values()))
    plt.xticks(fontsize=6)
    plt.xlabel('Features')
    plt.ylabel('Ecto Feature importance')

    mutula_i = {}
    for var in baye_best_model.nodes:
        if (var == 'label'):
            continue
        mutula_i.update({var: mutula_info_values(var, baye_best_model, {'label': 2})})
    print(mutula_i)
    plt.figure('endo_feartures_importance')
    plt.bar(list(mutula_i.keys()), list(mutula_i.values()))
    plt.xticks(fontsize=6)
    plt.xlabel('Features')
    plt.ylabel('Endo Feature importance')

    mutula_i = {}
    for var in baye_best_model.nodes:
        if (var == 'label'):
            continue
        mutula_i.update({var: mutula_info_values(var, baye_best_model, {'label': 3})})
    print(mutula_i)
    plt.figure('messo_feartures_importance')
    plt.bar(list(mutula_i.keys()), list(mutula_i.values()))
    plt.xticks(fontsize=6)
    plt.xlabel('Features')
    plt.ylabel('Messo Feature importance')

    plt.show()

    lab={'pluri':0}
    belief_propagation = BeliefPropagation(baye_best_model)
    for var in baye_best_model.nodes:
        if (var == 'label'):
           continue
        dist=belief_propagation.query(variables=[var], evidence={'label': list(lab.values())[0]})
        plt.figure(list(lab.keys())[0]+' '+var)
        x=np.linspace(0,len(dist.values),500)
        y=interpolate.make_interp_spline(dist.state_names[var], dist.values)(x)
        #plt.bar(dist.state_names[var], dist.values)
        plt.bar(x,y)
        plt.xlabel('Features')
        plt.ylabel('Probability')


    lab={'ecto':1}
    belief_propagation = BeliefPropagation(baye_best_model)
    for var in baye_best_model.nodes:
        if (var == 'label'):
           continue
        dist=belief_propagation.query(variables=[var], evidence={'label': list(lab.values())[0]})
        plt.figure(list(lab.keys())[0]+' '+var)
        x=np.linspace(0,len(dist.values),500)
        y=interpolate.make_interp_spline(dist.state_names[var], dist.values)(x)
        #plt.bar(dist.state_names[var], dist.values)
        plt.bar(x,y)
        plt.xlabel('Features')
        plt.ylabel('Probability')

    lab={'endo':2}
    belief_propagation = BeliefPropagation(baye_best_model)
    for var in baye_best_model.nodes:
        if (var == 'label'):
           continue
        dist=belief_propagation.query(variables=[var], evidence={'label': list(lab.values())[0]})
        plt.figure(list(lab.keys())[0]+' '+var)
        x=np.linspace(0,len(dist.values),500)
        y=interpolate.make_interp_spline(dist.state_names[var], dist.values)(x)
        #plt.bar(dist.state_names[var], dist.values)
        plt.bar(x,y)
        plt.xlabel('Features')
        plt.ylabel('Probability')


    lab={'messo':3}
    belief_propagation = BeliefPropagation(baye_best_model)
    for var in baye_best_model.nodes:
        if (var == 'label'):
           continue
        dist=belief_propagation.query(variables=[var], evidence={'label': list(lab.values())[0]})
        plt.figure(list(lab.keys())[0]+' '+var)
        x=np.linspace(0,len(dist.values),500)
        y=interpolate.make_interp_spline(dist.state_names[var], dist.values)(x)
        #plt.bar(dist.state_names[var], dist.values)
        plt.bar(x,y)
        plt.xlabel('Features')
        plt.ylabel('Probability')
    plt.show()

##############################################################################################################################



if __name__ == '__main__':
    main()