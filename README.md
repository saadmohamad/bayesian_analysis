**What is this repository for?**

Bayesian analysis, please refer to the paper figure 3


## Requirements

- pandas                    1.1.5
- h5py                      3.1.0
- numpy                     1.19.2
- pytorch                   1.6.0 
- pgmpy                     0.1.12
- networkx                  2.5
- scipy                     1.5.2
- scikit-learn              0.23.2
- scipy                     1.5.2
