import pandas as pd
import h5py
def creat_h5_datasets(filename,seq):
    import subprocess
    h5_file = filename + '.h5' if(not seq) else filename + 'seq.h5'
    num_lines = subprocess.check_output(['wc', '-l', filename])
    num_lines = int(num_lines.split()[0])-1
    firstline = pd.read_csv(filename, nrows=1)  # gets the first line
    num_features = firstline.size - 3
    #mask = np.ones(firstline.size, dtype=bool)
    #mask[-2]=0
    chunksize = 10000
    # this is your HDF5 database:
    with h5py.File(h5_file, 'w') as h5f:
        # use num_features-1 if the csv file has a column header
        dset1 = h5f.create_dataset('features',
                                   shape=(num_lines, num_features),
                                   compression=None,
                                   dtype='float32')
        dset2 = h5f.create_dataset('labels',
                                   shape=(num_lines,),
                                   compression=None,
                                   dtype='int')
        if(seq):
            dset3 = h5f.create_dataset('frame',
                                       shape=(num_lines,),
                                       compression=None,
                                       dtype='int')
            dset4 = h5f.create_dataset('Cell_id',
                                       shape=(num_lines,),
                                       compression=None,
                                       dtype='int')
        # change range argument from 0 -> 1 if your csv file contains a column header
        for i in range(0, num_lines, chunksize):
            df = pd.read_csv(filename,
                             nrows=chunksize,  # number of rows to read at each iteration
                             skiprows=i)  # skip rows that were already read

            #features = df.values[:, mask]
            #labels = df.values[:, -2]
            features = df.values[:, :num_features]
            labels = df.values[:, -1]
            frame = df.values[:, -2]
            cell_id=df.values[:, -3]


            # use i-1 and i-1+10 if csv file has a column header
            end = features.shape[0]
            #if(i+chunksize>=num_lines):
            #    end=end-1
            dset1[i:i + end, :] = features
            dset2[i:i +end] = labels
            if(seq):
                dset3[i:i + end] = frame
                dset4[i:i + end] = cell_id

def preprocess_input(img):

    preprocessed_img = img.detach().clone()
    input = preprocessed_img.requires_grad_(True)
    return input
